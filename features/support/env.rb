require 'rspec/expectations'
require 'rspec'
require 'rack/test'
require 'rspec/expectations'
require 'json'
require 'byebug'
require_relative '../../app'

include Rack::Test::Methods

def app
  Sinatra::Application
end

Before do | escenario |
  post '/fecha_fija', { :fecha => '2021-10-05' }.to_json, 'CONTENT_TYPE' => 'application/json'
end

After do | escenario |
  post '/reset', 'CONTENT_TYPE' => 'application/json'
end
