require 'byebug'

Dado('que ficho al jugador {string} con valor {int}') do |nombre, valor|
  body = {
    'nombre': nombre,
    'nacionalidad': 'argentino',
    'posicion': 'delantero',
    'valor_de_mercado': valor,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que ficho al jugador {string} de nacionalidad {string} con valor {int}') do |nombre, nacionalidad, valor|
  body = {
    'nombre': nombre,
    'nacionalidad': nacionalidad,
    'posicion': 'delantero',
    'valor_de_mercado': valor,
    'potencialidad': 3
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Cuando('consulto la lista de equipos') do
  get '/equipos'
end

Entonces('entonces tengo {int} equipos') do |cantidad_de_equipos_esperada|
  expect(last_response.status).to be == 200
  lista_equipos = JSON.parse(last_response.body)
  expect(lista_equipos.count).to eq cantidad_de_equipos_esperada
end

Entonces('el equipo {string} esta {string}') do |nombre, estado_esperado|
  expect(last_response.status).to be == 200
  lista_equipos = JSON.parse(last_response.body)
  lista_equipos.each do |equipo|
    expect(equipo['estado']).to eq estado_esperado if equipo['nombre'] == nombre
  end
end

Entonces('el presupuesto ejecutado del equipo es {int}') do |presupuesto_ejecutado_esperado|
  expect(last_response.status).to be == 200
  presupuesto_actual = JSON.parse(last_response.body)['presupuesto_ejecutado'].to_i
  expect(presupuesto_actual).to be == presupuesto_ejecutado_esperado
end
